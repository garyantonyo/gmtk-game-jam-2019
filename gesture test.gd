extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var hue = 0
var elapsed_time = 0
var timer = 0
var state = []
var gestures = [[0,true],[0,false]]
var isButtonPressed = false
var isButtonUnpressed = false
const GESTURE_LIST_LENGTH = 7

# what patterns do we want to gesture?
# switch this to have the last one being dynamically altered while its running.
# pattern: click 2, release 2, click - > [2,2]
var gesture_pattern = [ [[2,true],[2,false],[2,true],[2,false]] , [[1,true],[1,false],[1,true],[1,false]] ]


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _process(delta):
	
	elapsed_time += delta
	
	if abs(elapsed_time - round(elapsed_time)) < .05:
		red_square()
	else: 
		black_square()
	
#	if len(state) < 1:
#		state.append("default")
#
#	#print(state, delta)
#	match state[0]:
#		"beat":
#
#			#start timer
#			timer += delta
#			if timer > 1:
#				state.pop_front()
#				timer = 0
#			pass
#		"default":
#			bright_color_square(delta)
#			state.pop_front()
#	#print(state, delta)
#	pass


func bright_color_square(delta):
	get_node("Square").color = Color.from_hsv(hue,1,1)
	hue += delta/5
	if hue >= 1:
		hue -= 1
	pass

func red_square():
	get_node("Square").color = Color.from_hsv(0,1,1)
	
func black_square():
	get_node("Square").color = Color.from_hsv(0,0,0)

func _input(event):
	gesture()
	print(closest_gesture(), gestures)


func gesture():
	# detect input, store into list
	if Input.is_action_just_pressed("beat_input"):
		var new_time = 0
		gestures.append([elapsed_time,true])
		isButtonPressed = true
	
	elif Input.is_action_just_released("beat_input"):
		var new_time = 0
		gestures.append([elapsed_time,false])
		isButtonPressed = false
		
	while len(gestures) > GESTURE_LIST_LENGTH:
		gestures.pop_front()


func closest_gesture():
	var fit = []
	
	# collect the distance from each gesture
	for pattern in gesture_pattern:
		var differences = []
		var avg_difference = 0
		
		# if we do not have enough gestures, just dont check
		if len(pattern) > len(gestures):
			continue
		
		for i in range(len(pattern)):
			var pattern_index = (len(pattern) - 1) - i
			var offset = int(gestures[pattern_index][1])
			if pattern[pattern_index][1] == gestures[pattern_index-offset][1]:
				var pattern_time = pattern[pattern_index][0]
				var gesture_time = gestures[pattern_index-offset][0] - gestures[pattern_index-1-offset][0]
				avg_difference += abs(pattern_time - gesture_time)
		
		avg_difference /= len(pattern)
		
		fit.append(avg_difference)
		pass
		
	# find min avg difference
	var min_index = -1
	var min_value = 10000000000000000
	for i in range(len(fit)):
		if fit[i]<min_value:
			min_value = fit[i]
			min_index = i
	print(min_index, " ", min_value)
	return min_index
	
	
	
	
	
	
	
	
	
	
	
	
	