extends KinematicBody2D

# Declare member variables here. Examples:
var starting_x = 0
var current_x = 0
var has_moved = 0
var movement = Vector2(-(1750/10),0)

# Called when the node enters the scene tree for the first time.
func _ready():
	starting_x = self.position.x
	print(starting_x)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	move_and_slide(movement)
	current_x = self.position
	print(current_x.x)
	if (current_x.x < 0):
		self.position.x = starting_x


func _on_LoopTimer_timeout():
	pass # Replace with function body.
