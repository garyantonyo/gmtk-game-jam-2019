extends KinematicBody2D

var movement = Vector2(-(1750/10),0)
var collision = KinematicCollision2D

# Called when the node enters the scene tree for the first time.
func _ready():
	print(collision)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	collision = move_and_collide(movement)
	if collision != null:
		self.queue_free()
