extends Panel

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var x = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	get_node("Button").connect("pressed", self, "_on_Button_pressed")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Button_pressed():
	x += 1
	get_node("Label").text_list[0] = "HELLO! you have pressed this button " + str(x) + " times"
	pass # Replace with function body.
