extends Label

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var accum = 0
var text_list = ["",""]

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	process_text()
	text_list[1] = write_time()\
#	pass

func write_time():
	var timeDict = OS.get_time();
	var hour = timeDict.hour;
	var minute = timeDict.minute;
	var seconds = timeDict.second;
	return str(hour) + ":" + str(minute) + ":" + str(seconds)
	

func process_text():
	text = ""
	for line in text_list:
		text += line + "\n"